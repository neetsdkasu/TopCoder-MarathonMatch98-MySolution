Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim S As Integer = CInt(Console.ReadLine())
            Dim P As Integer = CInt(Console.ReadLine())
            Dim princesses(P - 1) As Integer
            For i As Integer = 0 To UBound(princesses)
                princesses(i) = CInt(Console.ReadLine())
            Next i
            Dim M As Integer = CInt(Console.ReadLine())
            Dim monsters(M - 1) As Integer
            For i As Integer = 0 To UBound(monsters)
                monsters(i) = CInt(Console.ReadLine())
            Next i
            Dim K As Integer = CInt(Console.ReadLine())

            Dim pam As New PrincessesAndMonsters()

            Dim retInit As String = pam.initialize(S, princesses, monsters, K)
            Console.Out.WriteLine(retInit)
            Console.Out.Flush()

            For st As Integer = 1 To S * S * S

                Dim nK As Integer = CInt(Console.ReadLine())
                Dim status(nK - 1) As Integer
                For i As Integer = 0 To UBound(status)
                    status(i) = CInt(Console.ReadLine())
                Next i

                Dim nP As Integer = CInt(Console.ReadLine())
                Dim nM As Integer = CInt(Console.ReadLine())
                Dim timeLeft As Integer = CInt(Console.ReadLine())

                Dim ret As String = pam.move(status, nP, nM, timeLeft)
                Console.Out.WriteLine(ret)
                Console.Out.Flush()

            Next st

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module