Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports TupI2 = System.Tuple(Of Integer, Integer)

Public Class PrincessesAndMonsters
    Const LeftTop     As Integer = 0
    Const RightTop    As Integer = 1
    Const RightBottom As Integer = 2
    Const LeftBottom  As Integer = 3
    Const Stay  As Integer = 0
    Const North As Integer = 1
    Const East  As Integer = 2
    Const South As Integer = 3
    Const West  As Integer = 4

    Dim Commands() As String = { "P", "N", "E", "S", "W" }

    Dim rand As New Random(19831983)

    Dim S As Integer
    Dim P As Integer
    Dim M As Integer
    Dim K As Integer
    Dim Krem As Integer
    Dim turn As Integer
    Dim phase As Integer
    Dim princess As New Point()
    Dim radiusP As Integer
    Dim radiusM As Integer
    Dim radius As Integer
    Dim dangerZone(3) As TupI2
    Dim army As New List(Of Corps)()
    Dim phaseTurn As Integer
    Dim braveCase As Boolean
    Dim corners(3) As Point

    Public Function initialize(S As Integer, princesses() As Integer, monsters() As Integer, K As Integer) As String

        Dim time0 As Integer = Environment.TickCount + 9800
        Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))

        Me.S = S
        Me.P = princesses.Length \ 2
        Me.M = monsters.Length \ 2
        Me.K = K
        Krem = K
        Console.Error.WriteLine( _
            "S: {0,2:D}, P: {1,2:D}, M: {2,3:D}, K: {3,3:D}, MaxScore: {4,4:D}" _
            , Me.S, Me.P, Me.M, Me.K, Me.P * 100 + Me.M _
            )
        turn = 0
        phase = 0
        phaseTurn = 0
        braveCase = M * 100 < S * S * 14
        
        corners(LeftTop) = New Point()
        corners(LeftTop).Row = 0
        corners(LeftTop).Col = 0
        corners(RightTop) = New Point()
        corners(RightTop).Row = 0
        corners(RightTop).Col = S - 1
        corners(RightBottom) = New Point()
        corners(RightBottom).Row = S - 1
        corners(RightBottom).Col = S - 1
        corners(LeftBottom) = New Point()
        corners(LeftBottom).Row = S - 1
        corners(LeftBottom).Col = 0
        
        CalcPrincessCenter(princesses)
        CalcDangerZone(monsters)
        
        Dim poss(K - 1) As Integer
        
        InitPhase0(poss)
        
        Dim ret As String = ""
        For i As Integer = 0 To K - 1
            ret += CStr(poss(i))
        Next i

        initialize = ret
    End Function

    Public Function move(status() As Integer, nP As Integer, nM As Integer, timeLeft As Integer) As String
        
        turn += 1
        phaseTurn += 1
        braveCase = nM * 100 < S * S * 14

        If timeLeft < 800 Then
            Dim earlyRet As String = ""
            If turn Mod 2 = 0 Then
                For i As Integer = 0 To K - 1
                    earlyRet += Commands(East)
                Next i
            Else
                For i As Integer = 0 To K - 1
                    earlyRet += Commands(North)
                Next i
            End If
            move = earlyRet
            Exit Function
        End If
        
        nP -= CheckLiveOrDead(status)
        
        Dim acts(K - 1) As Integer
        
        Select Case phase
        Case 0
            phase = Phase0(acts)
            If phase <> 0 Then
                phaseTurn = 0
            End If
        Case 1
            If phaseTurn = 1 Then
                InitPhase1()
            End If
            phase = Phase1(acts)
            If phase <> 1 Then
                phaseTurn = 0
            End If
        Case 2
            If phaseTurn = 1 Then
                InitPhase2(status)
            End If
            phase = Phase2(acts, nP)
            If phase <> 2 Then
                phaseTurn = 0
            End If
        Case 3
            If phaseTurn = 1 Then
                InitPhase3(status)
            End If
            phase = Phase3(acts)
            If phase <> 3 Then
                phaseTurn = 0
            End If
        Case 4
            If phaseTurn = 1 Then
                InitPhase4(status)
            End If
            phase = Phase4(acts)
            If phase <> 4 Then
                phaseTurn = 0
            End If
        Case 5
            If phaseTurn = 1 Then
                InitPhase5(status)
            End If
            phase = Phase5(acts, nM)
            If phase <> 5 Then
                phaseTurn = 0
            End If
        Case 6
            If phaseTurn = 1 Then
                InitPhase6(status)
            End If
            phase = Phase6(acts)
            If phase <> 6 Then
                phaseTurn = 0
            End If
        Case Else
            Throw New Exception("dame!")
        End Select
        
        Dim ret As String = ""
        For i As Integer = 0 To K - 1
            ret += Commands(acts(i))
        Next i

        move = ret
    End Function
    
    Class Point
        Public Row As Integer
        Public Col As Integer
    End Class
    
    Sub CalcPrincessCenter(princesses() As Integer)
        Dim Rmin As Integer = Integer.MaxValue
        Dim Cmin As Integer = Integer.MaxValue
        Dim Rmax As Integer = 0
        Dim Cmax As Integer = 0
        Dim Rsum As Integer = 0
        Dim Csum As Integer = 0
        
        For i As Integer = 0 To UBound(princesses)
            Dim v As Integer = princesses(i)
            If i Mod 2 = 0 Then
                Rsum += v
                Rmin = Math.Min(Rmin, v)
                Rmax = Math.Max(Rmax, v)
            Else
                Csum += v
                Cmin = Math.Min(Cmin, v)
                Cmax = Math.Max(Cmax, v)
            End If
        Next i
        
        Dim Ropt As Integer = 0
        Dim Copt As Integer = 0
        radiusP = Integer.MaxValue
        For r As Integer = Rmin To Rmax
            For c As Integer = Cmin To Cmax
                Dim tmpRadius As Integer = 0
                For i As Integer = 0 To P - 1
                    Dim dr As Integer = princesses(i * 2    ) - r
                    Dim dc As Integer = princesses(i * 2 + 1) - c
                    tmpRadius = Math.Max(tmpRadius, dr * dr + dc * dc)
                Next i
                If tmpRadius < radiusP Then
                    radiusP = tmpRadius
                    Ropt = r
                    Copt = c
                End If
            Next c
        Next r
        
        radiusP = Math.Max(1, radiusP)
        
        Dim Ravg As Integer = (Rsum + P \ 2) \ P
        Dim Cavg As Integer = (Csum + P \ 2) \ P
        Dim Rmed As Integer = (Rmax + Rmin) \ 2
        Dim Cmed As Integer = (Cmax + Cmin) \ 2
        
        princess.Row = (Ravg + Rmed + Ropt + 1) \ 3
        princess.Col = (Cavg + Cmed + Copt + 1) \ 3
        
        Console.Error.WriteLine( _
            "avg: ({0,2:D},{1,2:D}), " _
            + "med: ({2,2:D},{3,2:D}), " _
            + "opt: ({4,2:D},{5,2:D}), " _
            + "princesses: ({7,2:D},{6,2:D})" _
            , Cavg, Ravg _
            , Cmed, Rmed _
            , Copt, Ropt _
            , princess.Row _
            , princess.Col _
            )
        
    End Sub
    
    Sub CalcDangerZone(monsters() As Integer)
        Dim d(3) As Integer
        
        radiusM = Integer.MaxValue
        
        For i As Integer = 0 To M - 1
            Dim mr As Integer = monsters(i * 2)
            Dim mc As Integer = monsters(i * 2 + 1)
            Dim r As Integer = If(mr < princess.Row, 0, 1)
            Dim c As Integer = If(mc < princess.Col, 0, 1) Xor r
            d((r << 1) Or c) += 1
            Dim dr As Integer = mr - princess.Row
            Dim dc As Integer = mc - princess.Col
            radiusM = Math.Min(radiusM, dr * dr + dc * dc)
        Next i
        
        For i As Integer = 0 To 3
            dangerZone(i) = New TupI2(d(i), i)
        Next i
        
        radius = CInt(Math.Sqrt(CDbl(radiusP)))
        radius += CInt(Math.Sqrt(CDbl(radiusM)))
        radius \= 2
        If braveCase OrElse S >= 20 Then
            radius = Math.Min(radius + 2, CInt(Math.Sqrt(CDbl(radiusM)))) ' enlarge
        End If
        radius *= radius
        
        Console.Error.WriteLine( _
            "radiusP: {0}, radiusM: {1}, radius: {6}" _
            + Console.Error.NewLine _
            + "dangerZone: " _
            + " LeftTop: {2}," _
            + " RightTop: {3}," _
            + " LeftBottom: {4}," _
            + " RightBottom: {5}" _
            , CInt(Math.Sqrt(CDbl(radiusP))) _
            , CInt(Math.Sqrt(CDbl(radiusM))) _
            , d(0), d(1), d(2), d(3) _
            , CInt(Math.Sqrt(CDbl(radius))) _
            )
    End Sub
    
    Class Corps
        Public Pos As New Point()
        Public Knights As New List(Of Integer)()
        Public FootSteps As Stack(Of Integer)
        Public Act As Integer
    End Class
    
    Function CheckLiveOrDead(status() As Integer) As Integer
        Dim escorted As Integer = 0
        For Each cp As Corps In army
            Dim i As Integer = 0
            Do While i < cp.Knights.Count
                Dim kn As Integer = cp.Knights(i)
                If status(kn) < 0 Then
                    cp.Knights.RemoveAt(i)
                    Krem -= 1
                Else
                    i += 1
                    escorted += status(kn)
                End If
            Loop
        Next cp
        CheckLiveOrDead = escorted
    End Function
    
    Sub InitPhase0(poss() As Integer)
        Dim ks As New List(Of TupI2)()
        For i As Integer = 0 To 3
            Dim cp As New Corps()
            cp.Pos.Row = corners(i).Row
            cp.Pos.Col = corners(i).Col
            army.Add(cp)
            Dim dr As Integer = cp.Pos.Row - princess.Row
            Dim dc As Integer = cp.Pos.Col - princess.Col
            ks.Add(New TupI2(dr * dr + dc * dc, i))
        Next i
        ks.Sort()
        Dim mem As Integer = If(braveCase, 5, 15)
        Dim kcnt As Integer = K
        For i As Integer = 0 To 3
            Dim pos As Integer = ks(i).Item2
            If kcnt < mem Then
                Do While kcnt > 0
                    kcnt -= 1
                    poss(kcnt) = pos
                    army(pos).Knights.Add(kcnt)
                Loop
                Exit For
            Else
                For j As Integer = 1 To mem
                    kcnt -= 1
                    poss(kcnt) = pos
                    army(pos).Knights.Add(kcnt)
                Next j
                If kcnt < mem Then
                    Do While kcnt > 0
                        kcnt -= 1
                        pos = ks(kcnt Mod (i + 1)).Item2
                        poss(kcnt) = pos
                        army(pos).Knights.Add(kcnt)
                    Loop
                    Exit For
                End If
            End If
        Next i
        Do While kcnt > 0
            kcnt -= 1
            Dim pos As Integer = kcnt Mod 4
            poss(kcnt) = pos
            army(pos).Knights.Add(kcnt)
        Loop
        Console.Error.WriteLine( _
            "InitPhase0:" _
            + " LeftTop:{0,3:D}, RightTop:{1,3:D}," _
            + " RightBottom:{2,3:D}, LeftBottom:{3,3:D}" _
            , army(LeftTop).Knights.Count _
            , army(RightTop).Knights.Count _
            , army(RightBottom).Knights.Count _
            , army(LeftBottom).Knights.Count _
            )
    End Sub
    
    Function Phase0(acts() As Integer) As Integer
        Dim reached As Integer = 0
        For Each cp As Corps In army
            Dim dr As Integer = cp.Pos.Row - princess.Row
            Dim dc As Integer = cp.Pos.Col - princess.Col
            Dim act As Integer = 0
            If dr * dr + dc * dc < radius Then
                act = Stay
            ElseIf Math.Abs(dr) > Math.Abs(dc) Then
                If dr < 0 Then
                    dr += 1
                    cp.Pos.Row += 1
                    act = South
                Else
                    dr -= 1
                    cp.Pos.Row -= 1
                    act = North
                End If
            Else
                If dc < 0 Then
                    dc += 1
                    cp.Pos.Col += 1
                    act = East
                Else
                    dc -= 1
                    cp.Pos.Col -= 1
                    act = West
                End If
            End If
            For Each kn As Integer In cp.Knights
                acts(kn) = act
            Next kn
            If (dr * dr + dc * dc < radius) OrElse cp.Knights.Count = 0 Then
                reached += 1
            End If
        Next cp
        Phase0 = If(reached = army.Count, 1, 0)
    End Function
    
    Sub InitPhase1()
        Dim tmpArmy As New List(Of Corps)()
        
        Dim sz As Integer = 24
        
        For i As Integer = 0 To sz * 4 - 1
            Dim cp As New Corps()
            cp.Pos.Row = army(i Mod 4).Pos.Row
            cp.Pos.Col = army(i Mod 4).Pos.Col
            tmpArmy.Add(cp)
        Next i
        
        Dim mem As Integer = If(braveCase, 1, 1)
        
        For i As Integer = 0 To 3
            Dim kcnt As Integer = army(i).Knights.Count
            For j As Integer = 0 To sz - 1
                Dim a As Integer = j * 4 + i
                If kcnt < mem Then
                    Do While kcnt > 0
                        kcnt -= 1
                        tmpArmy(a).Knights.Add(army(i).Knights(kcnt))
                    Loop
                    Exit For
                Else
                    For e As Integer = 1 To mem
                        kcnt -= 1
                        tmpArmy(a).Knights.Add(army(i).Knights(kcnt))
                    Next e
                    If kcnt < mem Then
                        Do While kcnt > 0
                            kcnt -= 1
                            a = (kcnt Mod (j + 1)) * 4 + i
                            tmpArmy(a).Knights.Add(army(i).Knights(kcnt))
                        Loop
                        Exit For
                    End If
                End If
            Next j
            Do While kcnt > 0
                kcnt -= 1
                Dim a As Integer = (kcnt Mod sz) * 4 + i
                tmpArmy(a).Knights.Add(army(i).Knights(kcnt))
            Loop
        Next i
        
        army = tmpArmy
        
        Dim names() As String = { "LeftTop", "RightTop", "RightBottom", "LeftBottom" }
        Dim msg As String = ""
        For i As Integer = 0 To 3
            msg += Console.Error.NewLine + " " + names(i) + ": ("
            For j As Integer = 0 To sz - 1
                If j > 0 Then msg += ","
                msg += String.Format("{0,2:D}", army(j * 4 + i).Knights.Count)
            Next j
            msg += "),"
        Next i
        
        Console.Error.WriteLine( _
            "InitPhase1: {0}" _
            , msg _
            )
    End Sub
    
    Function Phase1(acts() As Integer) As Integer
        Dim reached As Integer = 0
        
        For i As Integer = 0 To army.Count - 1
            Dim dr As Integer = army(i).Pos.Row - princess.Row
            Dim dc As Integer = army(i).Pos.Col - princess.Col
            Dim act As Integer = Stay
            If dr = 0 AndAlso dc = 0 Then
                act = Stay
            Else
                Dim t As Integer = i \ 4
                If t = 0 Then
                    If Math.Abs(dr) > Math.Abs(dc) Then
                        If dr < 0 Then
                            dr += 1
                            army(i).Pos.Row += 1
                            act = South
                        Else
                            dr -= 1
                            army(i).Pos.Row -= 1
                            act = North
                        End If
                    Else
                        If dc < 0 Then
                            dc += 1
                            army(i).Pos.Col += 1
                            act = East
                        Else
                            dc -= 1
                            army(i).Pos.Col -= 1
                            act = West
                        End If
                    End If
                ElseIf t = 1 Then
                    If i Mod 2 = 0 Then
                        If dr < 0 Then
                            dr += 1
                            army(i).Pos.Row += 1
                            act = South
                        ElseIf dr > 0 Then
                            dr -= 1
                            army(i).Pos.Row -= 1
                            act = North
                        ElseIf dc < 0 Then
                            dc += 1
                            army(i).Pos.Col += 1
                            act = East
                        ElseIf dc > 0 Then
                            dc -= 1
                            army(i).Pos.Col -= 1
                            act = West
                        End If
                    Else
                        If dc < 0 Then
                            dc += 1
                            army(i).Pos.Col += 1
                            act = East
                        ElseIf dc > 0 Then
                            dc -= 1
                            army(i).Pos.Col -= 1
                            act = West
                        ElseIf dr < 0 Then
                            dr += 1
                            army(i).Pos.Row += 1
                            act = South
                        ElseIf dr > 0 Then
                            dr -= 1
                            army(i).Pos.Row -= 1
                            act = North
                        End If
                    End If                    
                Else
                    If t Mod 2 = 0 Then
                        If dr < -(t \ 2) Then
                            dr += 1
                            army(i).Pos.Row += 1
                            act = South
                        ElseIf dr > (t \ 2) Then
                            dr -= 1
                            army(i).Pos.Row -= 1
                            act = North
                        ElseIf dc < 0 Then
                            dc += 1
                            army(i).Pos.Col += 1
                            act = East
                        ElseIf dc > 0 Then
                            dc -= 1
                            army(i).Pos.Col -= 1
                            act = West
                        ElseIf dr < 0 Then
                            dr += 1
                            army(i).Pos.Row += 1
                            act = South
                        ElseIf dr > 0 Then
                            dr -= 1
                            army(i).Pos.Row -= 1
                            act = North
                        End If
                    Else
                        If dc < -(t \ 2) Then
                            dc += 1
                            army(i).Pos.Col += 1
                            act = East
                        ElseIf dc > (t \ 2) Then
                            dc -= 1
                            army(i).Pos.Col -= 1
                            act = West
                        ElseIf dr < 0 Then
                            dr += 1
                            army(i).Pos.Row += 1
                            act = South
                        ElseIf dr > 0 Then
                            dr -= 1
                            army(i).Pos.Row -= 1
                            act = North
                        ElseIf dc < 0 Then
                            dc += 1
                            army(i).Pos.Col += 1
                            act = East
                        ElseIf dc > 0 Then
                            dc -= 1
                            army(i).Pos.Col -= 1
                            act = West
                        End If
                    End If
                End If
            End If
            For Each kn As Integer In army(i).Knights
                acts(kn) = act
            Next kn
            If (dr = 0 AndAlso dc = 0) OrElse army(i).Knights.Count = 0 Then
                reached += 1
            End If
        Next i
        
        Phase1 = If(reached = army.Count, 2, 1)
    End Function
    
    Dim phase2size As Integer
    Dim phase2walk As Integer
    Dim phase2count As Integer
    Dim phase2size2 As Integer
    Dim phase2walk2 As Integer
    Dim phase2count2 As Integer
    Dim phase2move As Integer
    Sub InitPhase2(status() As Integer)
        
        phase2size = 1
        phase2walk = 0
        phase2count = 0

        phase2size2 = 1
        phase2walk2 = 0
        phase2count2 = 0
        phase2move = Math.Min(4, radiusP \ 2 + 1)
        
        Dim knights As New List(Of Integer)()
        For i As Integer = 0 To K - 1
            If status(i) >= 0 Then
                knights.Add(i)
            End If
        Next i
    
        If army.Count > 8 Then
            army.RemoveRange(8, army.Count - 8)
        Else
            Do While army.Count < 8
                army.Add(New Corps())
            Loop
        End If
        
        For i As Integer = 0 To 7
            army(i).Knights.Clear()
            army(i).FootSteps = New Stack(Of Integer)()
            army(i).Pos.Row = princess.Row
            army(i).Pos.Col = princess.Col
            army(i).Act = (i Mod 4) + 1
        Next i
        
        Dim mem As Integer = If(braveCase, 5, 7)
        Dim kcnt As Integer = knights.Count
        
        For i As Integer = 0 To 7
            If kcnt < mem Then
                Do While kcnt > 0
                    kcnt -= 1
                    army(i).Knights.Add(knights(kcnt))
                Loop
                Exit For
            Else
                For j As Integer = 1 To mem
                    kcnt -= 1
                    army(i).Knights.Add(knights(kcnt))
                Next j
                If kcnt < mem Then
                    Do While kcnt > 0
                        kcnt -= 1
                        Dim a As Integer = kcnt Mod (i + 1)
                        army(a).Knights.Add(knights(kcnt))
                    Loop
                    Exit For
                End If
            End If
        Next i
        Do While kcnt > 0
            kcnt -= 1
            Dim a As Integer = kcnt Mod 8
            army(a).Knights.Add(knights(kcnt))
        Loop
        
        Console.Error.WriteLine( _
            "InitPhase2:" _
            + " LeftTop: ({0,2:D},{1,2:D})," _
            + " RightTop: ({2,2:D},{3,2:D})," _
            + " RightBottom: ({4,2:D},{5,2:D})," _
            + " LeftBottom: ({6,2:D},{7,2:D})," _
            , army(0 + 0).Knights.Count _
            , army(4 + 0).Knights.Count _
            , army(0 + 1).Knights.Count _
            , army(4 + 1).Knights.Count _
            , army(0 + 2).Knights.Count _
            , army(4 + 2).Knights.Count _
            , army(0 + 3).Knights.Count _
            , army(4 + 3).Knights.Count _
            )
    End Sub
    
    Function Phase2(acts() As Integer, nP As Integer) As Integer
        
        ' nP += 100 ' ignored count of free princesses
        
        If phaseTurn < S + S + S Then

            phase2walk += 1
            If phase2walk = phase2size Then
                phase2walk = 0
                phase2count += 1
                If phase2count Mod 2 = 0 Then
                    phase2size += 1
                End If
            End If
        
            For i As Integer = 0 To 3
                Dim act As Integer = Stay
                Select Case army(i).Act
                Case North
                    If army(i).Pos.Row > 0 Then
                        army(i).Pos.Row -= 1
                        act = army(i).Act
                    End If
                Case East
                    If army(i).Pos.Col + 1 < S Then
                        army(i).Pos.Col += 1
                        act = army(i).Act
                    End If
                Case South
                    If army(i).Pos.Row + 1 < S Then
                        army(i).Pos.Row += 1
                        act = army(i).Act
                    End If
                Case West
                    If army(i).Pos.Col > 0 Then
                        army(i).Pos.Col -= 1
                        act = army(i).Act
                    End If
                End Select
                army(i).FootSteps.Push(act)
                For Each kn As Integer In army(i).Knights
                    acts(kn) = act
                Next kn
                If phase2walk = 0 Then
                    Select Case army(i).Act
                    Case North
                        army(i).Act = If(i \ 2 = 0, East, West)
                    Case East
                        army(i).Act = If(i \ 2 = 0, South, North)
                    Case South
                        army(i).Act = If(i \ 2 = 0, West, East)
                    Case West
                        army(i).Act = If(i \ 2 = 0, North, South)
                    End Select
                End If
            Next i
            
            phase2move -= 1
            If phase2move < 0 Then
                phase2walk2 += 1
                If phase2walk2 = phase2size2 Then
                    phase2walk2 = 0
                    phase2count2 += 1
                    If phase2count2 Mod 2 = 0 Then
                        phase2size2 += 1
                    End If
                End If
            End If

            For i As Integer = 4 To 7
                Dim act As Integer = Stay
                Select Case army(i).Act
                Case North
                    If army(i).Pos.Row > 0 Then
                        army(i).Pos.Row -= 1
                        act = army(i).Act
                    End If
                Case East
                    If army(i).Pos.Col + 1 < S Then
                        army(i).Pos.Col += 1
                        act = army(i).Act
                    End If
                Case South
                    If army(i).Pos.Row + 1 < S Then
                        army(i).Pos.Row += 1
                        act = army(i).Act
                    End If
                Case West
                    If army(i).Pos.Col > 0 Then
                        army(i).Pos.Col -= 1
                        act = army(i).Act
                    End If
                End Select
                army(i).FootSteps.Push(act)
                For Each kn As Integer In army(i).Knights
                    acts(kn) = act
                Next kn
                If phase2move < 0 AndAlso phase2walk2 = 0 Then
                    Select Case army(i).Act
                    Case North
                        army(i).Act = If(i \ 2 = 0, East, West)
                    Case East
                        army(i).Act = If(i \ 2 = 0, South, North)
                    Case South
                        army(i).Act = If(i \ 2 = 0, West, East)
                    Case West
                        army(i).Act = If(i \ 2 = 0, North, South)
                    End Select
                End If
            Next i
            Phase2 = If(np = 0, 6, 2)
        Else
            Dim reached As Integer = 0
            For i As Integer = 0 To 7
                Dim act As Integer = Stay
                If army(i).FootSteps.Count > 0 Then
                    act = army(i).FootSteps.Pop()
                    Select Case act
                    Case North
                        act = South
                        army(i).Pos.Row += 1
                    Case East
                        act = West
                        army(i).Pos.Col -= 1
                    Case South
                        act = North
                        army(i).Pos.Row -= 1
                    Case West
                        act = East
                        army(i).Pos.Col += 1
                    End Select
                End If
                For Each kn As Integer In army(i).Knights
                    acts(kn) = act
                Next kn
                If army(i).FootSteps.Count = 0 Then
                    reached += 1
                End If
            Next i
            Phase2 = If(reached = army.Count, 3, If(nP = 0, 6, 2))
        End If
    End Function
    
    Dim phase3target As New Point()
    Sub InitPhase3(status() As Integer)
        If army.Count > 1 Then
            army.RemoveRange(1, army.Count - 1)
            army(0).FootSteps = Nothing
            army(0).Knights.Clear()
        Else
            army.Add(New Corps())
        End If
        
        army(0).Pos.Row = princess.Row
        army(0).Pos.Col = princess.Col
        
        For i As Integer = 0 To K - 1
            If status(i) >= 0 Then
                army(0).Knights.Add(i)
            End If
        Next i
        
        Dim sel As Integer = 0
        Dim danger As Double = 1.0
        For Each t As TupI2 In dangerZone
            Dim d As Double = 2.0
            Select Case t.Item2
            Case LeftTop
                d = CDbl(t.Item1) / ((princess.Row + 1) * (princess.Col + 1))
            Case RightTop
                d = CDbl(t.Item1) / ((princess.Row + 1) * (S - princess.Col))
            Case RightBottom
                d = CDbl(t.Item1) / ((S - princess.Row) * (S - princess.Col))
            Case LeftBottom
                d = CDbl(t.Item1) / ((S - princess.Row) * (princess.Col + 1))
            End Select
            If d < danger Then
                danger = d
                sel = t.Item2
            End If
        Next t
        
        phase3target.Row = corners(sel).Row
        phase3target.Col = corners(sel).Col
        
        Console.Error.WriteLine( _
            "InitPhase3:" _
            + " Knights: {0,3:D}," _
            + " Target: ({1,2:D},{2,2:D})" _
            , army(0).Knights.Count _
            , phase3target.Col _
            , phase3target.Row _
            )
        
    End Sub
    
    Function Phase3(acts() As Integer) As Integer
        
        Dim dr As Integer = army(0).Pos.Row - phase3target.Row
        Dim dc As Integer = army(0).Pos.Col - phase3target.Col
        Dim escape As Boolean = (Math.Abs(dr) + Math.Abs(dc) = 1) AndAlso (army(0).Knights.Count >= 15)
        Dim act As Integer = Stay
        
        If Math.Abs(dr) > Math.Abs(dc) Then
            If dr < 0 Then
                If Not escape Then
                    army(0).Pos.Row += 1
                End If
                act = South
            Else
                If Not escape Then
                    army(0).Pos.Row -= 1
                End If
                act = North
            End If
        Else
            If dc < 0 Then
                If Not escape Then
                    army(0).Pos.Col += 1
                End If
                act = East
            Else
                If Not escape Then
                    army(0).Pos.Col -= 1
                End If
                act = West
            End If
        End If
        
        If escape Then
            For Each kn As Integer In army(0).Knights
                acts(kn) = Stay
            Next kn
            acts(army(0).Knights(0)) = act
            Phase3 = 4
        Else
            For Each kn As Integer In army(0).Knights
                acts(kn) = act
            Next kn
            Phase3 = 3
        End If
    End Function
    
    Sub InitPhase4(status() As Integer)
    End Sub
        
    Function Phase4(acts() As Integer) As Integer
        Dim S2 As Integer = S \ 2
        Dim reached As Integer = 0
        
        For Each cp As Corps In army
            Dim dr As Integer = cp.Pos.Row - S2
            Dim dc As Integer = cp.Pos.Col - S2
            Dim act As Integer = Stay
            
            If Math.Abs(dr) > Math.Abs(dc) Then
                If dr < 0 Then
                    dr += 1
                    cp.Pos.Row += 1
                    act = South
                Else
                    dr -= 1
                    cp.Pos.Row -= 1
                    act = North
                End If
            Else
                If dc < 0 Then
                    dc += 1
                    cp.Pos.Col += 1
                    act = East
                ElseIf dc > 0 Then
                    dc -= 1
                    cp.Pos.Col -= 1
                    act = West
                End If
            End If
            
            For Each kn As Integer In cp.Knights
                acts(kn) = act
            Next kn
            
            If dr = 0 AndAlso dc = 0 Then
                reached += 1
            End If
            
        Next cp
        
        Phase4 = If(reached = army.Count, 5, 4)
    End Function
    
    Dim phase5size As Integer
    Dim phase5walk As Integer
    Dim phase5count As Integer
    Sub InitPhase5(status() As Integer)
        
        phase5size = 1
        phase5walk = 0
        phase5count = 0
        
        Dim knights As New List(Of Integer)()
        For i As Integer = 0 To K - 1
            If status(i) >= 0 Then
                knights.Add(i)
            End If
        Next i
        
        Dim tmpArmy As New List(Of Corps)()
        For i As Integer = 0 To 7
            Dim cp As New Corps()
            cp.Pos.Row = army(0).Pos.Row
            cp.Pos.Col = army(0).Pos.Col
            cp.Act = (i Mod 4) + 1
            tmpArmy.Add(cp)
        Next i
        
        Dim mem As Integer = If(braveCase, 15, 20)
        Dim kcnt As Integer = knights.Count
        
        For i As Integer = 0 To 7
            If kcnt < mem Then
                Do While kcnt > 0
                    kcnt -= 1
                    tmpArmy(i).Knights.Add(knights(kcnt))
                Loop
                Exit For
            Else
                For j As Integer = 1 To mem
                    kcnt -= 1
                    tmpArmy(i).Knights.Add(knights(kcnt))
                Next j
                If kcnt < mem Then
                    Do While kcnt > 0
                        kcnt -= 1
                        Dim a As Integer = kcnt Mod (i + 1)
                        tmpArmy(a).Knights.Add(knights(kcnt))
                    Loop
                    Exit For
                End If
            End If
        Next i
        
        Do While kcnt > 0
            kcnt -= 1
            Dim a As Integer = kcnt Mod 8
            tmpArmy(a).Knights.Add(knights(kcnt))
        Loop
        
        army = tmpArmy
        
        Console.Error.WriteLine( _
            "InitPhase5:" _
            + " Pos: ({8,2:D},{9,2:D})," _
            + " Knights: ({0,2:D},{1,2:D},{2,2:D},{3,2:D}," _
            + "{4,2:D},{5,2:D},{6,2:D},{7,2:D})," _
            , army(0).Knights.Count _
            , army(1).Knights.Count _
            , army(2).Knights.Count _
            , army(3).Knights.Count _
            , army(4).Knights.Count _
            , army(5).Knights.Count _
            , army(6).Knights.Count _
            , army(7).Knights.Count _
            , army(0).Pos.Col _
            , army(0).Pos.Row _
            )
        
    End Sub
        
    Function Phase5(acts() As Integer, nM As Integer) As Integer
        
        phase5walk += 1
        If phase5walk = phase5size Then
            phase5walk = 0
            phase5count += 1
            If phase5count Mod 2 = 0 Then
                phase5size += 1
            End If
        End If
        
        Dim reached As Integer = 0
        
        For i As Integer = 0 To 7
            For Each kn As Integer In army(i).Knights
                acts(kn) = army(i).Act
            Next kn
            Select Case army(i).Act
            Case North
                army(i).Pos.Row -= 1
            Case East
                army(i).Pos.Col += 1
            Case South
                army(i).Pos.Row += 1
            Case West
                army(i).Pos.Col -= 1
            End Select
            If phase5walk = 0 Then
                Select Case army(i).Act
                Case North
                    army(i).Act = If(i \ 2 = 0, East, West)
                Case East
                    army(i).Act = If(i \ 2 = 0, South, North)
                Case South
                    army(i).Act = If(i \ 2 = 0, West, East)
                Case West
                    army(i).Act = If(i \ 2 = 0, North, South)
                End Select
            End If
            For j As Integer = 0 To 3
                Dim dr As Integer = army(i).Pos.Row - corners(j).Row
                Dim dc As Integer = army(i).Pos.Col - corners(j).Col
                If Math.Abs(dr) + Math.Abs(dc) = 1 Then
                    reached += 1
                    Exit For
                End If
            Next j
        Next i
        
        Phase5 = 5
        If reached > 0 AndAlso Krem >= 15 AndAlso nM > 0 Then
            If phaseTurn + 4 * S < S * S * S - turn - 1 Then
                Phase5 = 4
            End If
        End If
    End Function

    Sub InitPhase6(status() As Integer)
        Console.Error.WriteLIne("InitPhase6")
    End Sub
        
    Function Phase6(acts() As Integer) As Integer
        Dim reached As Integer = 0
        
        For Each cp As Corps In army
            Dim dr As Integer = cp.Pos.Row - princess.Row
            Dim dc As Integer = cp.Pos.Col - princess.Col
            Dim act As Integer = Stay
            If dr = 0 AndAlso dc = 0 Then
                act = Stay
            ElseIf Math.Abs(dr) > Math.Abs(dc) Then
                If dr < 0 Then
                    dr += 1
                    cp.Pos.Row += 1
                    act = South
                Else
                    dr -= 1
                    cp.Pos.Row -= 1
                    act = North
                End If
            Else
                If dc < 0 Then
                    dc += 1
                    cp.Pos.Col += 1
                    act = East
                Else
                    dc -= 1
                    cp.Pos.Col -= 1
                    act = West
                End If
            End If
            For Each kn As Integer In cp.Knights
                acts(kn) = act
            Next kn
            If dr = 0 AndAlso dc = 0 Then
                reached += 1
            End If
        Next cp
        
        Phase6 = If(reached = army.Count, 3, 6)
    End Function
    
End Class